from keras.models import Model, Sequential
from keras.layers import (BatchNormalization, Conv2D, MaxPooling2D, Activation,
                          Dropout, Lambda, Dense, Flatten, Input)

import tensorflow as tf
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
class FashionNet:
    @staticmethod
    def build_category_branch(inputs, numCategories, finalAct="softmax",
                              chanDim=-1):
        
        x = Lambda(lambda c: tf.image.rgb_to_grayscale(c))(inputs)
        
        # CONV => RELU => POOL
        x = Conv2D(32, (3,3), padding='same')(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D((3,3))(x)
        x = Dropout(0.25)(x)
        
        # (CONV => RELU)*2 => POOL
        x = Conv2D(64,(3,3),padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = Conv2D(64, (3,3), padding='same')(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D((3,3))(x)
        x = Dropout(0.25)(x)
        
        # (CONV => RELU)*2 => POOL
        x = Conv2D(128,(3,3),padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = Conv2D(128, (3,3), padding='same')(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D((3,3))(x)
        x = Dropout(0.25)(x)
        
        # define a branch of output layers for the number of different clothing
        # categories
        x = Flatten()(x)
        x = Dense(256)(x)
        x = Activation("relu")(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)
        x = Dense(numCategories)(x)
        x = Activation(finalAct, name='category_output')(x)
        
        return x
        pass
    
    @staticmethod
    def build_colour_branch(inputs, numColours, finalAct="softmax",
                            chanDim=-1):
        
        # CONV=> RELU => POOL
        x = Conv2D(16, (3,3), padding='same')(inputs)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D((3,3))(x)
        x = Dropout(0.25)(x)
        
        # CONV => RELU => POOL
        x = Conv2D(32,(3,3), padding="same")(x)
        x = Activation('relu')(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D((2,2))(x)
        x = Dropout(0.25)(x)
        
        # CONV => RELU => POOL
        x = Conv2D(32,(3,3), padding="same")(x)
        x = Activation('relu')(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D((2,2))(x)
        x = Dropout(0.25)(x)
        
        # define a branch of output layers for the different colours
        x = Flatten()(x)
        x = Dense(128)(x)
        x = Activation('relu')(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)
        x = Dense(numColours)(x)
        x = Activation(finalAct, name='colour_output')(x)
        
        return x
        pass
    
    @staticmethod
    def build(width, height, numCategories, numColours, finalAct="softmax"):
        
        inputShape = (height, width, 3)
        chanDim = -1
        
        inputs = Input(shape=inputShape)
        categoryBranch = FashionNet.build_category_branch(inputs, numCategories,
                                                          finalAct, chanDim=chanDim)
        colourBranch = FashionNet.build_colour_branch(inputs, numColours,
                                                      finalAct, chanDim = chanDim)
        
        model = Model(
            inputs=inputs,
            outputs=[categoryBranch,colourBranch],
            name="fashion_net")
        
        return model
        pass