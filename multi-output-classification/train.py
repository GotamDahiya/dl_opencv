import matplotlib
matplotlib.use('Agg')

from keras.optimizers import SGD, Adam
from keras.preprocessing.image import img_to_array
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from pyimagesearch.fashionet import FashionNet
from imutils import paths

import matplotlib.pyplot as plt
import numpy as np
import argparse, os, cv2, random, pickle
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

ap = argparse.ArgumentParser()
ap.add_argument("-d","--dataset",required=True,
                help="path to input dataset")
ap.add_argument("-m","--model", required=True,
                help="path to output model")
ap.add_argument("-l","--categorybin",required=True,
                help="path to output category label binarizer")
ap.add_argument("-c","--colourbin",required=True,
                help="path to out colour labal binarizer")
ap.add_argument("-p","--plot",type=str,default="output",
                help="base filename for generated plots")
args = vars(ap.parse_args())

EPOCHS = 50
INIT_LR = 1e-3
BS = 32
IMAGE_DIMS = (96,96,3)

print("[INFO] loading images")
imagePaths = sorted(list(paths.list_images(args["dataset"])))
random.seed(42)
random.shuffle(imagePaths)

data = []
categoryLabels = []
colourLabels = []

for imagePath in imagePaths:
    
    image = cv2.imread(imagePath)
    image = cv2.resize(image, (IMAGE_DIMS[1], IMAGE_DIMS[0]))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = img_to_array(image)
    data.append(image)
    
    # extract the clothing colour and category from the path and update the
    # respective lists
    (colour, cat) = imagePath.split(os.path.sep)[-2].split("_")
    categoryLabels.append(cat)
    colourLabels.append(colour)
    pass

# scale the raw pixel intensities to the range [0, 1] and convert to a NumPy
# array
data = np.array(data, dtype="float") / 255.0
print("[INFO] data matrix: {} images ({:.2f}MB)".format(len(imagePaths), 
                                                       data.nbytes/(1024*1000.0)))

categoryLabels = np.array(categoryLabels)
colourLabels = np.array(colourLabels)

print("[INFO] binarizing labels")
categoryLB = LabelBinarizer()
categoryLabels = categoryLB.fit_transform(categoryLabels)

colourLB = LabelBinarizer()
colourLabels = colourLB.fit_transform(colourLabels)

split = train_test_split(data, categoryLabels, colourLabels, test_size=0.2,
                         random_state=42)

(trainX,testX, trainCategoryX, testCategoryY, trainColourX, testColourY) = split

model = FashionNet.build(96,96,numCategories=len(categoryLB.classes_),
                         numColours=len(colourLB.classes_),
                         finalAct="softmax")

losses = {
    "category_output" : "categorical_crossentropy",
    "colour_output" : "categorical_crossentropy"
    }
lossWeights = {"category_output":1.0, "colour_output":1.0}

print('[INFO] compiling model')
opt = Adam(lr=INIT_LR, decay=INIT_LR/EPOCHS)
model.compile(optimizer=opt, loss=losses, loss_weights=lossWeights,
              metrics=["accuracy"])

history = model.fit(x=trainX,
                    y={"category_output":trainCategoryX,"colour_output":trainColourX},
                    validation_data=(testX,
                                     {"category_output":testCategoryY,"colour_output":testColourY}),
                    epochs=EPOCHS,
                    verbose=1)

print("[INFO] serializing network")
model.save(args["model"], save_format="h5")

print("[INFO] serializing category label binarizer")
f = open(args["categorybin"],"wb")
f.write(pickle.dumps(categoryLB))
f.close()

print("[INFO] serializing colour label binarizer")
f = open(args["colourbin"],"wb")
f.write(pickle.dumps(colourLB))
f.close()

lossNames = ["loss", "category_output_loss", "colour_output_loss"]
plt.style.use("ggplot")
(fig, ax) = plt.subplots(3, 1, figsize=(13, 13))
# loop over the loss names
for (i, l) in enumerate(lossNames):
	# plot the loss for both the training and validation data
	title = "Loss for {}".format(l) if l != "loss" else "Total loss"
	ax[i].set_title(title)
	ax[i].set_xlabel("Epoch #")
	ax[i].set_ylabel("Loss")
	ax[i].plot(np.arange(0, EPOCHS), history.history[l], label=l)
	ax[i].plot(np.arange(0, EPOCHS), history.history["val_" + l],
		label="val_" + l)
	ax[i].legend()
# save the losses figure
plt.tight_layout()
plt.savefig("{}_losses.png".format(args["plot"]))
plt.close()

accuracyNames = ["category_output_accuracy", "colour_output_accuracy"]
plt.style.use("ggplot")
(fig, ax) = plt.subplots(2, 1, figsize=(8, 8))
# loop over the accuracy names
for (i, l) in enumerate(accuracyNames):
	# plot the loss for both the training and validation data
	ax[i].set_title("Accuracy for {}".format(l))
	ax[i].set_xlabel("Epoch #")
	ax[i].set_ylabel("Accuracy")
	ax[i].plot(np.arange(0, EPOCHS), history.history[l], label=l)
	ax[i].plot(np.arange(0, EPOCHS), history.history["val_" + l],
		label="val_" + l)
	ax[i].legend()
# save the accuracies figure
plt.tight_layout()
plt.savefig("{}_accs.png".format(args["plot"]))
plt.close()