from pyimagesearch.colourdescriptor import ColourDescriptor
import argparse, glob, cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d","--dataset",required=True,
                help="path to the directory that contains the images to be indexed")
ap.add_argument("-i","--index",required=True,
                help="path to where the computed index will be stored")
args = vars(ap.parse_args())

# initialize the color descriptor
cd = ColourDescriptor((8,12,3))

# open the index file
output = open(args["index"],"w")

# using glob to grab the images from dataset
for imagePath in glob.glob(args["dataset"]+"/*.png"):
    
    # extract the image ID
    imageID = imagePath[imagePath.rfind("/")+1:]
    image = cv2.imread(imagePath)
    
    # describe the image
    features = cd.describe(image)
    
    # wrute the features to file
    features = [str(f) for f in features]
    output.write("%s,%s\n" % (imageID,",".join(features)))
    pass

output.close()