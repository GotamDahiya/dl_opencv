import numpy as np
import cv2
import imutils

class ColourDescriptor:
    def __init__(self, bins):
        # store the number of bins for the histogram        
        self.bins = bins
        
    def describe(self, image):
        
        # convert the image from BGR to HSV and initialize the features for indexing
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        features = []
        
        # grab the dimensions and compute the center of the image
        h,w = image.shape[:2]
        cX, cY = (int(w*0.5), int(h*0.5))
        
        # dividing the image into four segments(top-left, top-right, bottom-right,
        # bottom-left)
        segments = [(0, cX, 0, cY), (cX, w, 0, cY), (cX, w, cY, h),
                    (0,cX,cY,h)]
        
        # construct the elliptical mask for center segment
        (axesX,axesY) = (int(w*0.75)//2, int(h*0.75)//2)
        ellipMask = np.zeros(image.shape[:2], dtype="uint8")
        cv2.ellipse(ellipMask, (cX,cY), (axesX,axesY), 0, 0, 360, 255, -1)
        
        # loop over the elements
        for (startX,endX,startY,endY) in segments:
            
            # construsting a mask for each corner segment, subtracting the 
            # elliptical mask from each
            cornerMask = np.zeros(image.shape[:2],dtype="uint8")
            cv2.rectangle(cornerMask, (startX,startY),(endX,endY),255,-1)
            cornerMask = cv2.subtract(cornerMask,ellipMask)
            
            # extract a colour histogram from the image and update the feature vector
            hist = self.histogram(image, cornerMask)
            features.extend(hist)
            
            return features
            pass
        pass
    
    def histogram(self,image,mask):
        # extract a 3D colour histogram from the masked region of the image
        hist = cv2.calcHist([image],[0,1,2],mask,self.bins,
                            [0, 180, 0, 256, 0, 256])
        
        # normalizing the histogram
        hist = cv2.normalize(hist, hist).flatten()
            
        return hist
        pass