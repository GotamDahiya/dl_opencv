import numpy as np
import csv

class Searcher:
    def __init__(self, indexPath):
        self.indexPath = indexPath
        
    def search(self, queryFeatures, limit=10):
        
        results = {}
        
        # open the index file for reading
        with open(self.indexPath) as f:
            
            reader = csv.reader(f)
            
            for row in reader:
                
                # parse out the imageId and features, then compute the chi-squared
                # distance between the features in the index and query features
                features = [float(x) for x in row[1:]]
                d = self.chi2_distance(features, queryFeatures)
                
                results[row[0]] = d
                pass
            
            f.close()
            pass
        results = sorted([(v,k) for (k,v) in results.items()])
        return results[:limit]
        pass
    
    def chi2_distance(self, histA, histB, eps=1e-10):
        
        # compute the chi-squared distance
        d = 0.5* np.sum([((a-b)**2)/(a+b+eps) for (a,b) in zip(histA,histB)])
        return d
        pass