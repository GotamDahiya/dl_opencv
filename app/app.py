# search route
import os, cv2

from flask import Flask, render_template, request, jsonify
from skimage import io

from pyimagesearch.colourdescriptor import ColourDescriptor
from pyimagesearch.searcher import Searcher

INDEX = os.path.join(os.path.dirname(__file__),'index.csv')

@app.route('/search', methods=["POST"])
def search():
    if request.method == "POST":
        
        RESULTS_ARRAY = []
        image_url = request.form.get('img')
        
        try:
            cd = ColourDescriptor((8,12,3))
            query = io.imread(image_url)
            query = (query*255).astype("uint8")
            r,g,b = cv2.split(query)
            query = cv2.merge([b,g,r])
            features = cd.describe(query)
            
            searcher = Searcher(INDEX)
            results = searcher.search(features)
            
            for score,resultID in results:
                RESULTS_ARRAY.append(
                    {"image":str(resultID),"score":str(score)})
                pass
            
            return jsonify(results=(RESULTS_ARRAY[:3]))
        except:
            
            jsonify({"sorry":"Sorry, no results found"}), 500
    pass   