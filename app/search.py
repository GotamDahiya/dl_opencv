from pyimagesearch.colourdescriptor import ColourDescriptor
from pyimagesearch.searcher import Searcher
import argparse, cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i","--index",required=True,
                help="path to where the computed index is stored")
ap.add_argument("-q","--query",required=True,
                help="Path to query image")
ap.add_argument("-r","--result-path",required=True,
                help="Path to result graph")
args = vars(ap.parse_args())

cd = ColourDescriptor((8,12,3))

# load the image and describe it
query = cv2.imread(args["query"])
features = cd.describe(query)

# perform the search
searcher = Searcher(args["index"])
results = searcher.search(features)

# display the query
cv2.imshow("Query",query)

for (score,resultID) in results:
    # load the result image and display it
    result = cv2.imread(args["result_path"]+"/"+resultID)
    cv2.imshow("Result",result)
    cv2.waitKey(0)
    pass