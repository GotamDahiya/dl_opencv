import matplotlib
matplotlib.use("Agg")

import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.preprocessing.image import img_to_array
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split
from pyimagesearch.smallervggnet import SmallerVGGNet
from imutils import paths

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import argparse, os, cv2, pickle, random, warnings
warnings.simplefilter("ignore")

# taking arguments from command line
ap = argparse.ArgumentParser()
ap.add_argument("-d","--dataset",required=True,
                help="path to model")
ap.add_argument("-m","--model",required=True,
                help="path to output model")
ap.add_argument("-l","--labelbin",required=True,
                help="path to output label binarizer")
ap.add_argument("-p","--plot",type=str,default="plot.png",
                help="path to output plot")
args = vars(ap.parse_args())

EPOCHS = 30
INIT_LR = 1e-3
BS = 32
IMAGE_DIMS = (96,96,3)

print("[INFO] loading images")
imagePaths = sorted(list(paths.list_images(args["dataset"])))
random.seed(42)
random.shuffle(imagePaths)

data, labels = [], []

for imagepath in imagePaths:
    # Reading image from disk
    image = cv2.imread(imagepath)
    image = cv2.resize(image,(IMAGE_DIMS[1],IMAGE_DIMS[0]))
    image = img_to_array(image)
    data.append(image)
    
    # extracting class labels from the image path
    l = label = imagepath.split(os.path.sep)[-2].split("_")
    labels.append(l)
    pass

# scale the raw pixel values to [0,1]
data = np.array(data, dtype="float")/255.0
labels = np.array(labels)
print("[INFO] data matrix: {} images ({:.2f}MB)".format(len(imagePaths),data.nbytes/(1024*1000.0)))

print("[INFO] class labels:")
mlb = MultiLabelBinarizer()
labels = mlb.fit_transform(labels)

for i,label in enumerate(mlb.classes_):
    print("{}. {}".format(i+1,label))
    pass

# Splitting data into train and validation datasets
X_train,X_val,y_train,y_val = train_test_split(data, labels,test_size=0.2,random_state=42)

aug = ImageDataGenerator(rotation_range=25,width_shift_range=0.1,
                         height_shift_range=0.1,shear_range=0.2,zoom_range=0.2,
                         horizontal_flip=True,fill_mode="nearest")
aug_test = ImageDataGenerator()

train_generator = aug.flow(X_train,y_train,batch_size=BS)
val_generator = aug_test.flow(X_val,y_val)

# Initializing the model using a softmax activation in the final layer
print("[INFO] compiling model")
model = SmallerVGGNet.build(
    width=IMAGE_DIMS[1],height=IMAGE_DIMS[0],
    depth=IMAGE_DIMS[2],classes=len(mlb.classes_),finalAct="softmax")

# Initializing the optimizer
opt = Adam(lr=INIT_LR, decay=INIT_LR/EPOCHS)

# compiling the model
model.compile(loss="binary_crossentropy",optimizer=opt,metrics=["accuracy"])

# train the network
print("[INFO] train the network")
history = model.fit(
    train_generator,
    steps_per_epoch = train_generator.n//BS,
    validation_data=val_generator,
    epochs=EPOCHS)

print("[INFO] serializing the network")
model.save(args["model"],save_format="h5")

print("[INFO] serializing label binarizer")
f = open(args["labelbin"], "wb")
f.write(pickle.dumps(mlb))
f.close()

plt.style.use("ggplot")
plt.figure()
N = EPOCHS
plt.plot(np.arange(0,N),history.history["loss"],label="Training loss")
plt.plot(np.arange(0,N),history.history["val_loss"],label="Validation Loss")
plt.plot(np.arange(0,N),history.history["accuracy"],label="Train Accuracy")
plt.plot(np.arange(0,N),history.history["val_accuracy"],label="Validation Accuracy")
plt.title("Training and Validation Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")
plt.savefig(args["plot"])