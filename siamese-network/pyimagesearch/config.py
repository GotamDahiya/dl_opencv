# importing the necessary pacakges
import os

# specifing the image size
IMG_SHAPE = (28,28,1)

# specifying batch size and number of epochs
BATCH_SIZE = 64
EPOCHS = 10

# define path to output directory
BASE_OUTPUT = "output"

MODEL_PATH = os.path.sep.join([BASE_OUTPUT, "siamese_model"])
PLOT_PATH = os.path.sep.join([BASE_OUTPUT, "plot.png"])

