from tensorflow.keras.models import (Model)
from tensorflow.keras.layers import (Input, Conv2D, Dense, Dropout, GlobalAveragePooling2D, MaxPooling2D)
from tensorflow.keras.callbacks import TensorBoard

def build_siamese_model(inputShape, embeddingDim = 48):

    inputs = Input(inputShape)

    X = Conv2D(64, (2,2), padding="same", activation='relu')(inputs)
    X = MaxPooling2D(pool_size=(2,2))(X)
    X = Dropout(0.3)(X)

    X = Conv2D(64, (2,2), padding="same", activation="relu")(X)
    X = MaxPooling2D(pool_size=(2,2))(X)
    X = Dropout(0.3)(X)

    X = GlobalAveragePooling2D()(X)
    outputs = Dense(embeddingDim)(X)

    model = Model(inputs, outputs)

    return model
    pass