import tensorflow.keras.backend as K
import matplotlib.pyplot as plt
import numpy as np

def make_pairs(images, labels):

    pairImages, pairLabels = [], []

    numClasses = len(np.unique(labels))
    idx = [np.where(labels == i)[0] for i in range(0, numClasses)]

    for idxA in range(len(images)):

        currentImg = images[idxA]
        label = labels[idxA]

        idxB = np.random.choice(idx[label])
        posImage = images[idxB]

        pairImages.append([currentImg, posImage])
        pairLabels.append([1])

        negIdx = np.where(labels != label)[0]
        negImg = images[np.random.choice(negIdx)]

        pairImages.append([currentImg, negImg])
        pairLabels.append([0])
        pass

    return (np.array(pairImages), np.array(pairLabels))
    pass

def euclidean_distance(vectors):

    (featA, featB) = vectors

    sumSquared = K.sum(K.square(featA - featB), axis=1, keepdims=True)

    return K.sqrt(K.maximum(sumSquared, K.epsilon()))
    pass

def plot_training(history, plotPath):

    plt.style.use("ggplot")
    plt.figure()

    plt.plot(history.history['loss'], label='Training Loss')
    plt.plot(history.history['val_loss'], label='Validation Loss')
    plt.plot(history.history['accuracy'], label='Training Accuracy')
    plt.plot(history.history['val_accuracy'], label='Validation Accuracy')

    plt.title('Training and Validation Loss and Accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Loss/Accuracy')

    plt.legend(loc='lower left')
    plt.savefig(plotPath)
    pass
    