# training using transfer learning using CNN
# importing the necessary packages
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import to_categorical

from sklearn.metrics import classification_report
from pyimagesearch import config

import numpy as np
import pickle, os

def csv_feature_generator(inputPath, bs, numClasses, mode="train"):
    # open the input file for reading
    f = open(inputPath, "r")
    
    # loop indefinitely
    while True:
        data, labels = [], []
        
        while len(data)<bs:
            # attempt to read the next row of the CSV file
            row = f.readline()
            
            # check to see if the row is empty, indicating we reached the end of the file
            if row == "":
                # reset the file pointer to the beginning of the file and re-read the file
                f.seek(0)
                f.readline()
                
                # if we are evaluating, we should now break from the loop to ensrure we don't
                # contribute to fill up the batch from samples at the beginning of the file
                if mode == "eval":
                    break
        
        # extract the class label and features from the row
        row = row.strip().split(",")
        label = row[0]
        label = to_categorical(label, num_classes=numClasses)
        features = np.array(row[1:], dtype="float")
        
        # update the data and label lists
        data.append(features)
        labels.append(label)
        pass
    
    yield (np.array(data),np.array(labels))
    pass

# load the label encoder from the disk
le = pickle.loads(open(config.LE_PATH, "rb").read())

# derive the paths to training, validation and testing csv files
trainPath = os.path.sep.join([config.BASE_CSV_PATH,"{}.csv".format(config.TRAIN)])
valPath =os.path.sep.join([config.BASE_CSV_PATH, "{}.csv".format(config.VAL)])
testPath = os.path.sep.join([config.BASE_CSV_PATH, "{}.csv".format(config.TEST)])

# determine the total number of images in the training and validation sets
totalTrain = sum([1 for l in open(trainPath)])
totalVal = sum([1 for l in open(valPath)])

# extract the testing labesl from the csv file and then determine the number of testing images
testLabels = [int(row.split(",")[0]) for row in open(testPath)]
totalTest = len(testLabels)

trainGen = csv_feature_generator(trainPath, config.BATCH_SIZE,len(config.CLASSES), mode="train")
testGen = csv_feature_generator(testPath, config.BATCH_SIZE, len(config.CLASSES), mode="eval")
valGen = csv_feature_generator(valPath, config.BATCH_SIZE, len(config.CLASSES), mode="eval")

# defining the neural network
model = Sequential()
model.add(Dense(256, input_shape=(7*7*2048,), activation="relu"))
model.add(Dense(12, activation="relu"))
model.add(Dense(len(config.CLASSES), activation="relu"))

# compiling the model
print('[INFO] compiling the model')
opt = SGD(lr=1e-3, momentum=0.9, decay=1e-3/25)
model.compile(loss="binary_crossentropy", optimizer=opt, metrics=["accuracy"])

# train the network
print("[INFO] training the network")
history = model.fit(x=trainGen,
                    steps_per_epoch = totalTrain // config.BATCH_SIZE,
                    validation_data = valGen,
                    validation_steps= totalVal // config.BATCH_SIZE,
                    epochs=25)

print('[INFO] evaluating network')
predIdxs = model.predict(x=testGen,
                         steps=(totalTest//config.BATCH_SIZE) + 1)
predIdxs = np.argmax(predIdxs, axis=1)
print(classification_report(testLabels, predIdxs, target_names = le.classes_))