import os

# initialize the path to the priginal input directory
ORIG_INPUT_DATASET = "Food-5K"

# initialize the base path to the new directory that will contain the images after computing the
# training and testing split.
BASE_PATH = "dataset"

# define the names of the training, testing and validation directories
TRAIN = "training"
TEST = "evaluation"
VAL = "validation"

# initialise the classes
CLASSES = ["food","non-food"]

# setting the batch size
BATCH_SIZE = 32

LE_PATH = os.path.sep.join(["output","le.cpickle"])
BASE_CSV_PATH = "output"

# set up path to the serialised model after training
MODEL_PATH = os.path.sep.join(["output","model.cpickle"])