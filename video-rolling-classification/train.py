import matplotlib
matplotlib.use("Agg")

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.layers import (AveragePooling2D, Dropout, Flatten, Dense,
                                     Input)
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import SGD

from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from imutils import paths

import matplotlib.pyplot as plt
import numpy as np
import argparse, pickle, cv2, os
import tensorflow as tf

# tf.keras.backend.clear_session()
os.environ["CUDA_VISIBLE_DEVICES"]="-1"
ap = argparse.ArgumentParser()
ap.add_argument("-d","--dataset", required=True,
                help="path to input dataset")
ap.add_argument("-m","--model",required=True,
                help="path to output serialized model")
ap.add_argument("-l","--label-bin",required=True,
                help="path to output label binarizer")
ap.add_argument("-e","--epochs", type=int, default=25,
                help="# of epochs to train the network")
ap.add_argument("-p","--plot",type=str, default="plot.png",
                help="path to output loss/accuracy plot")
args = vars(ap.parse_args())

LABELS = set(["weight_lifting","tennis","football"])

print("[INFO] loading images")
imagePaths = list(paths.list_images(args["dataset"]))
data = []
labels = []

# loop over the image paths
for imagepath in imagePaths:
    # extract the label from filename
    label = imagepath.split(os.path.sep)[-2]
    
    # if the label of the current images is not found ignore the label
    if label not in LABELS:
        continue
    
    image = cv2.imread(imagepath)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224,244))
    
    # update the data and labels lists
    data.append(image)
    labels.append(label)
    pass

data, labels = np.array(data), np.array(labels)

# perform one hot encoding on the labels
lb = LabelBinarizer()
labels = lb.fit_transform(labels)

# splitting the data into train and test split using 75% for training and 
# 25% for testing
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25,
                                                  stratify=labels, random_state=42)

trainAug = ImageDataGenerator(rotation_range=30,
                              zoom_range=0.15,
                              width_shift_range=0.2,
                              height_shift_range=0.2,
                              shear_range=0.15,
                              horizontal_flip=True,
                              fill_mode="nearest")

valAug = ImageDataGenerator()

# define the ImageNet mean subtraction (in RGB) and set the mean subtraction
#  value for each of the data augmentation objects
mean = np.array([123.68, 116.779, 103.939], dtype="float32")
trainAug.mean = mean
valAug.mean = mean

baseModel = ResNet50(weights="imagenet", include_top=False,
                     input_tensor=Input(shape=(224, 224, 3)))

# construct the head of the model that will be placed on top of the base model
headModel = baseModel.output
headModel = AveragePooling2D(pool_size=(7,7))(headModel)
headModel = Flatten(name="flatten")(headModel)
headModel = Dense(512, activation='relu')(headModel)
headmodel = Dropout(0.5)(headModel)
headModel = Dense(len(lb.classes_), activation='softmax')(headModel)

# place the FC model on top of the base model
model = Model(inputs=baseModel.input, outputs=headModel)

for layer in baseModel.layers:
    layer.trainable = False
    pass

# compiling the model (this needs to be done after setting the the layers to non-trainable)
print("[INFO] compiling model")
opt = SGD(lr=1e-4, momentum=0.9, decay=1e-4/args["epochs"])
model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

print("[INFO] training head")
history = model.fit(x=trainAug.flow(trainX, trainY, batch_size=32),
                    steps_per_epoch=len(trainX)//32,
                    validation_data = valAug.flow(testX,testY),
                    validation_steps = len(testX)//32,
                    epochs=args["epochs"])

print("[INFO] evaluating model")
predictions = model.predict(x=testX.astype("float32"), batch_size=32)
print(classification_report(testY.argmax(axis=1),
                            predictions.argmax(axis=1), target_names=lb.classes_))

N = args["epochs"]
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0,N), history.history["loss"], label="train_loss")
plt.plot(np.arange(0,N), history.history["val_loss"], label="val_loss")
plt.plot(np.arange(0,N), history.history["accuracy"], label="train_acc")
plt.plot(np.arange(0,N), history.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy on dataset")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(args["plot"])

print("[INFO] serializing network")
model.save(args["model"], save_format="h5")

f = open(args["label_bin"], "wb")
f.write(pickle.dumps(lb))
f.close()    