#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 23:22:18 2020

@author: gotam_dahiya
"""


import uuid
import os

class TempImage:
    
    def __inin__(self,basePath="./",ext=".jpg"):
        
        self.path = "{base_path}/{rand}{ext}".format(base_path=basePath,
                                                     rand=str(uuid.uuid4()),ext=ext)
        pass
    
    def cleanup(self):
        os.remove(self.path)