from pyimagesearch.TempImage import TempImage
from imutils.video import VideoStream
import dropbox
import argparse
import datetime
import time
import imutils
import cv2
import warnings
import json

ap = argparse.ArgumentParser()
ap.add_argument("-c","--conf",required=True,
                help="Path to json file")
args = vars(ap.parse_args())

warnings.filterwarnings("ignore")
conf = json.load(open(args["conf"]))
client = None

print("[INFO] warming up")
vs = cv2.VideoCapture(0)
time.sleep(conf["camera_warmup_time"])
avg = None
lastUploaded = datetime.datetime.now()
motioncounter = 0

while True:
    
    _, frame = vs.read()
    timestamp = datetime.datetime.now()
    text = "unoccupied"
    
    frame = imutils.resize(frame,width=500)
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (21,21), 0)
    
    if avg is None:
        print("[INFO] starting background model")
        avg = gray.copy().astype("float")
        continue
    
    cv2.accumulateWeighted(gray,avg,0.5)
    frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))
    
    thresh = cv2.threshold(frameDelta,conf["delta_thresh"],255,
                           cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh,None,iterations=2)
    cnts = cv2.findContours(thresh.copy(),cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    
    for c in cnts:
        if cv2.contourArea(c) < conf["min_area"]:
            continue
        
        (x,y,w,h) = cv2.boundingRect(c)
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        text="Occupied"
        pass
    
    ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
    cv2.putText(frame,"Room Status: {}".format(text), (10,20),
                cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,0),2)
    cv2.putText(frame,ts,(10,frame.shape[0]-10),cv2.FONT_HERSHEY_SIMPLEX,0.5,
                (0,0,255),1)
    
    if conf["show_video"]:
        
        cv2.imshow("Feed",frame)
        cv2.imshow("Thresh",thresh)
        cv2.imshow("Frame Delta",frameDelta)
        key = cv2.waitKey(1) & 0xFF
        
        if key == ord('q'):
            break
    pass

cv2.destroyAllWindows()