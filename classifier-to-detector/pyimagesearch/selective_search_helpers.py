'''
Helper function to find the region of interests in an image using selective
search instead of image pyramids and sliding windows
'''
import time
import cv2

def find_regions(orig,method):
    
    # initialize OpenCV's selective search implementation and set the
    # input image
    ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()
    ss.setBaseImage(orig)
    
    # check to see which version of selective search is being used
    if method == "fast":
        print("[INFO] using *fast* selective search")
        ss.switchToSelectiveSearchFast()

    else:
        print("[INFO] using *quality* selective search")
        ss.switchToSelectiveSearchQuality()
    
    # Finding the Region of Interests using the
    start = time.time()
    rects = ss.process()
    end = time.time()
    print("[INFO] time taken for selective search {:.5f}".format(end-start))
    
    for (x,y,w,h) in rects:
        
        yield (x,y,w,h)
        pass
    pass