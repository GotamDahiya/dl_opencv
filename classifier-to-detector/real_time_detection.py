import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import keras
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.applications.resnet import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications import imagenet_utils
from imutils.object_detection import non_max_suppression
from pyimagesearch.detection_helpers import sliding_window
from pyimagesearch.detection_helpers import image_pyramid
from imutils.video import VideoStream
from imutils.video import FPS
from pyimagesearch.selective_search_helpers import find_regions

import numpy as np
import argparse
import imutils
import time
import cv2

keras.backend.clear_session()


ap = argparse.ArgumentParser()
ap.add_argument("-c","--min-conf",type=float,default=0.9,
                help="minimum probablity to filter weak detections")
args = vars(ap.parse_args())

WIDTH = 600
PYR_SCALE = 1.5
WIN_STEP = 16
# ROI_SIZE = eval(args["size"])
INPUT_SIZE = (224, 224)

print("[INFO] Loading ResNet model")
model = ResNet50(weights="imagenet",include_top=True)

print("[INFO] starting video stream")
vs = VideoStream(src=0).start()
time.sleep(2.0)
fps = FPS().start()

while True:
    
    frame = vs.read()
    frame = cv2.resize(frame,(224,224))
    H, W = frame.shape[:2]
    
    locs,rois = [],[]
    method = "fast"
    
    for (x,y,w,h) in find_regions(frame,method):
        
        if w/float(W) < 0.40 or h/float(H) < 0.40:
            continue
        
        # Extracting ROI from image
        roi = frame[y:y+h,x:x+w]
        roi = cv2.cvtColor(roi,cv2.COLOR_BGR2RGB)
        roi = cv2.resize(roi,(224,224))
        
        # Preprocessing the images
        roi = img_to_array(roi)
        roi = preprocess_input(roi)
        
        # Adding images and location of bounding boxes to the lists
        rois.append(roi)
        locs.append((x,y,x+w,y+h))
        pass
    
    rois = np.array(rois,dtype="float")
    
    preds = model.predict(rois)
    
    preds = imagenet_utils.decode_predictions(preds,top=1)
    labels = {}
    
    for (i,p) in enumerate(preds):
        
        # Extracting the predicted information
        (imagenetId, label, prob) = p[0]
        
        if prob >= args["min_conf"]:
            box = locs[i]
            
            L = labels.get(label,[])
            L.append((box,prob))
            labels[label] = L
        pass
    
    for label in labels.keys():
        
        boxes = np.array([p[0] for p in labels[label]])
        proba = np.array([p[1] for p in labels[label]])
        boxes_1 = non_max_suppression(boxes,proba)
        
        for (startX,startY,endX,endY) in boxes_1:
            # Extracting the probabilty associated with each bounding box
            boxes_2 = np.array([startX,startY,endX,endY])
            idx = np.where((boxes == boxes_2).all(axis = 1))[0][0]
            prob = proba[idx]
            print(label,prob)
            # draw the bounding box and label on the image
            cv2.rectangle(frame, (startX, startY), (endX, endY),
                          (0, 255, 0), 2)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            label_1 = "{}: {:.2f}%".format(label,prob*100)
            cv2.putText(frame, label_1, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 255, 0), 2)
            pass
        pass
    
    cv2.imshow("Frame",frame)
    key = cv2.waitKey(1) & 0xFF
    
    if key == ord('q'):
        break
    
    fps.update()
    time.sleep(10.0)
    pass

fps.stop()

print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

cv2.destroyAllWindows()
vs.stop()