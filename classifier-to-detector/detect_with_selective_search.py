# import the necessary packages
import os
os.environ['CUDA_VISIBLE_DEVICES'] = "-1"

import keras
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications import imagenet_utils
from imutils.object_detection import non_max_suppression
from pyimagesearch.selective_search_helpers import find_regions

import numpy as np
import cv2
import argparse
import time
import imutils

keras.backend.clear_session()

ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required=True,
    help="Path to input image")
ap.add_argument("-m","--method",type=str,default="fast",
    choices=["fast","quality"],
    help="selective search method")
ap.add_argument("-c", "--min-conf", type=float, default=0.9,
	help="minimum probability to filter weak detections")
ap.add_argument("-v", "--visualize", type=int, default=-1,
	help="whether or not to show extra visualizations for debugging")
ap.add_argument("-f","--filter",type=str, default=None,
                help="Comma separated list of ImageNet labels to filter on")
args = vars(ap.parse_args())

# Extracting the filters given by user
label_filters = args["filter"]

if label_filters is not None:
    label_filters = label_filters.lower().split(",")

# Loading the network 
print("[INFO] Loading network")
model = VGG16(weights="imagenet",include_top=True)

# Loading the image as specified by the user
orig = cv2.imread(args["image"])
(H, W) = orig.shape[:2]

# Storing the regions of interests and labels corresponding to those regions.
# rects = find_regions(orig,args["method"])
locs,rois = [], []
start = time.time()

for (x,y,w,h) in find_regions(orig,args["method"]):
    
    # Take the roi from the bounding box locations and preprocess it for 
    # classification
    
    if w/float(W) < 0.25 or h/float(H) < 0.25:
        continue
    
    # Extracting the images
    roi = orig[y:y+h, x:x+w]
    roi = cv2.cvtColor(roi,cv2.COLOR_BGR2RGB)
    roi = cv2.resize(roi,(224,224))
    
    # Preprocessing extracted images for model
    roi = img_to_array(roi)
    roi = preprocess_input(roi)
    
    # updating list of ROIs and respective locations
    rois.append(roi)
    locs.append((x,y,x+w,y+h))
    
    if args["visualize"] > 0:
        # clone the original image and then draw a bounding box
		# surrounding the current region
        clone = orig.copy()
        cv2.rectangle(clone, (x, y), (x + w, y + h),
			(0, 255, 0), 2)

        # show the visualization and current ROI
        cv2.imshow("Visualization", clone)
        cv2.imshow("ROI",orig[y:y+h,x:x+h])
        cv2.waitKey(1)
        # time.sleep(0.25)
    pass

# How long it took to loop over all the ROIs found using the selective search
# algorithm
end = time.time()
print("[INFO] looping over ROIs took {:.5f} seconds".format(end-start))

rois = np.array(rois, dtype="float32")

# Classify each of the propsed ROIs using the ResNet model and then show how the
# classification looks
print("[INFO] classifying ROIs")
start = time.time()
preds = model.predict(rois)
end = time.time()
print("[INFO] classifying ROIs took {:.5f} seconds".format(end-start))

# decoding the predictions and initializong a dictionary that maps the decoded
# prediction to the corresponding label
preds = imagenet_utils.decode_predictions(preds,top=1)
labels = {}

# Loop over the predictions
for (i, p) in enumerate(preds):
    
    # Extracting the prediction information
    (imagenetId, label, prob) = p[0]
    
    if label_filters is not None and label not in label_filters:
        continue
    
    # filter out the weak detections by ensuring the predicted probability
    # is greater than the minimum confidence value
    if prob >= args["min_conf"]:
        # Grabing the bounding box associated with that ROI
        box = locs[i]
        
        # grab the list of predictions for the label and add the bounding box 
        # and probability to the list of labels
        L = labels.get(label,[])
        L.append((box,prob))
        labels[label] = L

    pass

# Loop over the labels for each of the detected objects
for label in labels.keys():
    
    print("[INFO] showing the results for {}".format(label))
    clone = orig.copy()
    
    # loop over all the bounding boxes associated with this label
    for (box,prob) in labels[label]:
        
        # Drawing the bounding box for the label
        (startX,startY,endX,endY) = box
        cv2.rectangle(clone,(startX,startY),(endX,endY),(0,255,0),2)
        pass
    
    # showing the results before applying non-max-suppression
    cv2.imshow("Before", clone)
    clone = orig.copy()
    
    # extract the bounding boxes and associated prediction
	# probabilities, then apply non-maxima suppression
    boxes = np.array([p[0] for p in labels[label]])
    proba = np.array([p[1] for p in labels[label]])
    boxes_1 = non_max_suppression(boxes, proba)
    
    # loop over all bounding boxes that were kept after applying
    # non-maxima suppression
    for (startX, startY, endX, endY) in boxes_1:
        
        # Extracting the probabilty associated with each bounding box
        boxes_2 = np.array([startX,startY,endX,endY])
        idx = np.where((boxes == boxes_2).all(axis = 1))[0][0]
        prob = proba[idx]
        
        # draw the bounding box and label on the image
        cv2.rectangle(clone, (startX, startY), (endX, endY),
			(0, 255, 0), 2)
        y = startY - 10 if startY - 10 > 10 else startY + 10
        label_1 = "{}: {:.2f}%".format(label,prob*100)
        cv2.putText(clone, label_1, (startX, y),
			cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 255, 0), 2)
        pass
    
    # show the output after apply non-maxima suppression
    cv2.imshow("After", clone)
    cv2.waitKey(0)
    pass