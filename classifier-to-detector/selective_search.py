import argparse 
import random
import cv2
import time

ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required=True,
    help="Path to input image")
ap.add_argument("-m","--method",type=str,default="fast",
    choices=["fast","quality"],
    help="selective search method")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])

ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()
ss.setBaseImage(image)

if args["method"] == "fast":
    print("[INFO] using *fast* selective search")
    ss.switchToSelectiveSearchFast()

else:
    print("[INFO] using *quality* selective search")
    ss.switchToSelectiveSearchQuality()
    
start = time.time()
rects = ss.process()
end = time.time()
print(rects.shape)
print(rects[0])
print("[INFO] selective search took {:.4f} seconds".format(end - start))
print("[INFO] {} total regions proposals".format(len(rects)))

for i in range(0,len(rects),100):
    
    output= image.copy()
    
    for (x,y,w,h) in rects[i:i+100]:
        color = [random.randint(0,255) for j in range(0,3)]
        cv2.rectangle(output,(x,y),(x+w,y+h),color,2)
        pass
    
    cv2.imshow("Output",output)
    key = cv2.waitKey(1) & 0xFF
    
    if key == ord('q'):
        break
    
    time.sleep(0.5)
    pass
    