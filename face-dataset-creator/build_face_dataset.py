'''
Usage
-----
python[3] buil_face_dataset.py [cascade file] [output directory]

Press 's' to save the image and 'q' to quit
'''

from imutils.video import VideoStream
import argparse
import imutils
import time
import cv2
import os

print(__doc__)

ap = argparse.ArgumentParser()
ap.add_argument("-c","--cascade",required=True,
                help="path to cascade file(.xml)")
ap.add_argument("-o","--output",required=True,
                help="path to dataset directory")
args = vars(ap.parse_args())

if not os.path.exists(args["output"]):
        os.makedirs(args["output"])

detector = cv2.CascadeClassifier(args["cascade"])

print("[INFO] starting video stream")
vs = VideoStream(src=0).start()
time.sleep(2.0)
total = 0

while True:
    
    frame = vs.read()
    orig = frame.copy()
    frame = imutils.resize(frame,width=400)
    
    rects = detector.detectMultiScale(
		cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY), scaleFactor=1.1, 
		minNeighbors=5, minSize=(30, 30))
    
    for (x,y,w,h) in rects:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2)
        pass
    
    cv2.imshow("Frame",frame)
    key = cv2.waitKey(1) & 0xFF
    
    if key == ord('s'):
        p = os.path.join(args["output"],"{}.png".format(str(total).zfill(5)))
        cv2.imwrite(p, orig)
        total += 1
    elif key == ord('q'):
        break
    pass

print("[INFO] {} face images stored".format(total))
print("[INFO] cleaning up")
vs.stop()
cv2.destroyAllWindows()