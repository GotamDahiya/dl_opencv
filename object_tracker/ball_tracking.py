from collections import deque
from imutils.video import VideoStream

import numpy as np
import cv2, imutils, time, argparse

ap = argparse.ArgumentParser()
ap.add_argument("-b","--buffer",type=int,default=64)

args = vars(ap.parse_args())

pts = deque(maxlen=args["buffer"])
counter = 0
(dX,dY) = (0,0)
direction=""

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

vs = VideoStream(src=0).start()
time.sleep(2.0)

while True:
    
    frame = vs.read()
    
    # frame = frame[1]
    frame - cv2.flip(frame, 1)
    if frame is None:
        print("[ERROR] camera not capturing any frames")
        break
    
    frame = imutils.resize(frame,width=600)
    # frame = cv2.cvtColor(frame,cv2.COLOR_RGB2BGR)
    blurred = cv2.GaussianBlur(frame,(11,11),0)
    faces = face_cascade.detectMultiScale(blurred,1.3,5)
    center = None
    
    for (x,y,w,h) in faces:
        
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        cv2.circle(frame,(int(x+w/2),int(y+h/2)),2,(0,255,0),1)
        center = (int(x+w/2),int(y+h/2))
        pts.appendleft(center)
        pass
    
    for i in np.arange(1,len(pts)):
        
        if pts[i-1] is None or pts[i] is None:
            continue
        
        thickness = int(np.sqrt(args["buffer"]/float(i+1)) * 2.5)
        cv2.line(frame,pts[i-1],pts[i],(0,0,255),thickness)
        pass
    cv2.imshow("Frame",frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    pass

vs.stop()
cv2.destroyAllWindows()