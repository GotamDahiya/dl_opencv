# importing necessary packages
from pyimagesearch.Sudoku.puzzle import extract_digit
from pyimagesearch.Sudoku.puzzle import find_puzzle
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from sudoku import Sudoku

import solve_puzzle
import numpy as np
import argparse, imutils, cv2, os
os.environ["CUDA_VISIBLE_DEVICES"]="-1"

# construct the argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-m","--model", required=True,
                help="path to trained digit classifier")
ap.add_argument("-i","--image", required=True,
                help="path to input sudoku puzzle image")
ap.add_argument("-d","--debug", type=int, default=-1,
                help="whether or not we are visualising each step of the pipeline")
args = vars(ap.parse_args())

# load the digit classifier from disk
print("[INFO] loading digit classifier")
model = load_model(args["model"])

# load the input image from disk and resize it
print("[INFO] processing image")
image = cv2.imread(args["image"])
image = imutils.resize(image, width=600)

# find the puzzle in the image
(puzzleImg, warped) = find_puzzle(image, debug=args["debug"]>0)

# initialize a 9*9 Sudoku board
board = np.zeros((9,9), dtype='int')

# a Sudoku puzzle is a 9*9 grid, so the location of each cell can be inferredby dividing the 
# warped image into a 9*9 grid
stepX = warped.shape[1] // 9
stepY = warped.shape[0] // 9

# initialize a list to store the (x, y) coordinates of each cell location
cellLocs = []

# loop over the grid locations
for y in range(0, 9):
    # initialize the current list of cell locations
    row = []

    for x in range(0, 9):
        # compute the starting and ending (x, y)-coordinates of the
        # current cell
        startX = x * stepX
        startY = y * stepY
        endX = (x + 1) * stepX
        endY = (y + 1) * stepY

        # add the (x, y)-coordinates to our cell locations list
        row.append((startX, startY, endX, endY))

        # crop the cell from the warped transform image and then
        # extract the digit from the cell
        cell = warped[startY:endY, startX:endX]
        digit = extract_digit(cell, debug=args["debug"] > 0)

        # verify that the digit is not empty
        if digit is not None:
            # foo = np.hstack([cell, digit])
            # cv2.imshow("Cell/Digit", foo)

            # resize the cell to 28x28 pixels and then prepare the
            # cell for classification
            roi = cv2.resize(digit, (28, 28))
            roi = roi.astype("float") / 255.0
            roi = img_to_array(roi)
            roi = np.expand_dims(roi, axis=0)

            # classify the digit and update the sudoku board with the
            # prediction
            pred = model.predict(roi).argmax(axis=1)[0]
            board[y, x] = pred
        if digit is None:
            board[y,x] = 0
    # add the row to our cell locations
    cellLocs.append(row)

print("[INFO] OCR'd sudoku board")
board = board.tolist()
print(board)

arr = solve_puzzle.solvePuzzle(board)

print(arr)
    
# solve the sudoku puzzle
# print("[INFO] solving sudoku puzzle...")
# solution = Sudoku(3, 3, board=board.tolist()).solve()
# solution.show_full()

# # loop over the cell locations and board
# for (cellrow, boardRow) in zip(cellLocs, board):
#     # loop over individual cell in the row
#     for (box, digit) in zip(cellrow, boardRow):
#         # unpack the coordinates
#         startX, startY, endX, endY = box
        
#         # compute the coordinates of where the digit will be drawn
#         textX = int((endX - startX) * 0.33)
#         textY = int((endY - startY) * -0.2)
#         textX += startX
#         textY += endY
        
#         # draw the result digit on the Sudoku puzzle image
#         cv2.putText(puzzleImg, str(digit), (textX, textY),
#                     cv2.FONT_HERSHEY_SIMPLEX, 0.9, (255,0,255), 2)
#         pass
#     pass

# cv2.imshow("Sudoku result", puzzleImg)
# cv2.waitKey(0)