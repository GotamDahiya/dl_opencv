# importing the necessary packages
# import numpy as np
import collections

def print_grid(arr):
    for i in range(9):
        for j in range(9):
            print(arr[i][j],end=' ')
            pass
        print()
        pass
    pass

def find_empty_location(arr, l):
    for row in range(9):
        for col in range(9):
            if(arr[row][col]==0):
                l[0]=row
                l[1]=col
                return True
            pass
        pass
    return False
    pass

def usedInRow(arr, row, num):
    for col in range(9):
        if(arr[row][col]==num):
            return True
        pass
    return False
    pass

def usedInCol(arr, col, num):
    for row in range(9):
        if(arr[row][col]==num):
            return True
        pass
    return False
    pass

def usedInBox(arr, row, col, num):
    for i in range(3):
        for j in range(3):
            if(arr[row+i][col+i]==num):
                return True
            pass
        pass
    return False
    pass

def isSafe(arr, row, col, num):
    return not usedInBox(arr,row-row%3,col-col%3, num) and not usedInCol(arr, col, num) and not usedInRow(arr, row, num)
    pass

def solvePuzzle(arr):
    l = [0,0]
    if(not find_empty_location(arr, l)):
        return True
    
    row,col = l
    loading = ['/','|','\\']
    for num in range(1,10):
        if isSafe(arr, row, col, num):
            print("\r{}".format(loading[num%3]),end='')
            arr[row][col]=num
            
            if(solvePuzzle(arr)):
                return True
            
            arr[row][col] = 0
        pass
    
    return False
    pass

def pass_solution(arr):
    print(arr)
    if(solvePuzzle(arr)):
        print("[DEBUG]")
        print(arr)
        return arr
    pass

if __name__ == '__main__':
    grid =[[0 for x in range(9)]for y in range(9)]
	
	# assigning values to the grid 
    grid = [[8, 0, 0, 0, 1, 0, 0, 0, 9], 
		[0, 5, 0, 8, 0, 7, 0, 1, 0], 
		[0, 0, 4, 0, 9, 0, 7, 0, 0], 
		[0, 6, 0, 7, 0, 1, 0, 2, 0], 
		[5, 0, 8, 0, 6, 0, 1, 0, 7], 
		[0, 1, 0, 5, 0, 2, 0, 9, 0], 
		[0, 0, 7, 0, 4, 0, 6, 0, 0], 
		[0, 8, 0, 3, 0, 9, 0, 4, 0], 
		[3, 0, 0, 0, 5, 0, 0, 0, 8]] 
    grid2 = [[8, 0, 0, 0, 1, 0, 0, 0, 9], [0, 5, 0, 8, 0, 7, 0, 7, 0], [0, 0, 4, 0, 9, 0, 7, 0, 0], [0, 6, 0, 7, 0, 1, 0, 2, 0], [5, 0, 8, 0, 6, 0, 1, 0, 7], [0, 1, 0, 5, 0, 2, 0, 9, 0], [0, 0, 7, 0, 4, 0, 8, 0, 0], [0, 8, 0, 3, 0, 9, 0, 4, 0], [3, 0, 0, 0, 5, 0, 0, 0, 8]]
    assert sorted(grid) == sorted(grid2)
    # if success print the grid 
    if(solvePuzzle(grid)): 
        print_grid(grid) 
    else:
        # print(solvePuzzle(grid))
        print("No solution exists")