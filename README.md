# Computer Vision with Deep Learning and Python

This repository contains all the applications of various blog posts from PyImageSearch, a website founded by Dr Adrian Rosebrock. It currently focuses on the deep learning aspects of computer vision. There are a few projects dealing with real-life application of OpenCV with Python

## Getting Started

Follow the proceeding instructions to use the repository in one's project.

### Prerequistes

Install these software for a smooth run
```
Python v3.7.5 or greater
```
Libraries required for the project
```
Numpy
Tensorflow v2
dlib
face_recognition
```

### Installing

Clone the repository on the local machine.
```
git clone https://gitlab.com/GotamDahiya/dl_opencv.git
```
In case one wants to update the files please feel free to fork the repository and make a pull request to update the changed file.

## Modules

Serial Number | Folder | Explanation
------------- | ------ | -----------
1 | basic-motion-detection | Motion Detection using difference in contours between consecutive frames.
2 | classifier-to-detector | Turining a CNN classifier into an object detector using Selective Search and Region Proposal methods.
3 | face-dataset-creator | Creating dataset of an individual's face, can be extended for any other object. For creating annotations, use an appropiat tool.
4 | face-recognition | Using OpenCV's dnn modules to create a face recognition script which can be used in real time.
5 | face-recognition-opencv | Face recognition by serializing the faces needed to be identified and then being compared to them.
6 | motion-detection | Similar to the first folder but the weighted mean is used and then the difference between that and the next frame is calculated. If above a certain threshold mark as motion.
7 | multi-label-classification | Multiple labels are generated for an image. The dataset is divided up on the basis of the colour and class of the images.
8 | multi-output-classification | Multiple labels are generated using 2 different CNN models, one for colour and the other for class label. This allows for identifying unknown images nt present in the classes in the directory.
9 | object-detection-deep-learning | Identifying objects in real time video streams using OpenCV's caffe models.
10 | training-rcnn-keras | Creating a R-CNN model in keras for identifying an object in an image. It also contains a script for finding objects in a directory of images, filtering the ones without an image.
11 | video-rolling-classification | Classifying videos using a rolling average, that is, classify each frame and take of the average of the probabilities of previous frames abd classify the video.
12 | sudoku-solver | Solving a sudoku puzzle by recognising the digits and running a backtracking algorithm on it. It displays the solved puzzle.
13 | character-recognition-tf | Training a custom OCR model with Keras and Tensorflow.
14 | transfer-learning | Using a classifier trained on dataset to be used on a different dataset. The features are extracted from a fully convolutional model before the Flatten layer. This is being used in Faster R-CNN models.
15 | HelperScripts | This folder contains a script for obtaining images from the internet using Bing API and a script for finding the colour range of an objet using a bitwise mask. It can find ranges for RGB and HSV.

## Built With

* **Python v3.7.5** 
* **Tensorflow v2.0**
* **Keras**

## Authors
* **Gotam Dahiya** - [Gotam Dahiya](https://gitlab.com/GotamDahiya)