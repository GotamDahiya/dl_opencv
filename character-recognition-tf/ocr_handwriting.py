# importing the necessary packages
from tensorflow.keras.models import load_model
from imutils.contours import sort_contours

import numpy as np
import argparse, imutils, cv2, os
os.environ["CUDA_VISIBLE_DEVICES"]="-1"

# constructing the argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-i","--image",required=True,
                help="path to input image")
ap.add_argument("-m","--model",type=str,required=True,
                help="path to trained model")
args = vars(ap.parse_args())

# loading the OCR model
print("[INFO] loading handwriting OCR model")
model = load_model(args["model"])

# loading input image from disk, converting it to grayscale, and blurring to reduece noise
image = cv2.imread(args["image"])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (5,5), 0)

# performing egde detection
edged = cv2.Canny(blurred, 30, 150)
cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sort_contours(cnts, method="left-to-right")[0]

# initialize the list of contour bounding boxes and associated characters that we'll be recognising
chars = []

# looping over contours
for c in cnts:
    
    (x,y,w,h) = cv2.boundingRect(c)
    
    # filter out the bounding boxes, ensuring they are neither too small or too large
    if (w>=5 and w<=150) and (h>=15 and h<=120):
        # extract the character and threshold it to make the character appear as white on a black
        # background
        roi = gray[y:y+h,x:x+w]
        thresh = cv2.threshold(roi, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
        (tH,tW) = thresh.shape
        
        if tW > tH:
            thresh = imutils.resize(thresh, width=32)
        else:
            thresh = imutils.resize(thresh, height=32)
            
        (tH, tW) = thresh.shape
        dx = int(max(0,32-tW)/2.0)
        dy = int(max(0,32-tH)/2.0)
        
        padded = cv2.copyMakeBorder(thresh, top=dy, bottom=dy, left=dx, right=dx,
                                    borderType=cv2.BORDER_CONSTANT, value=(0,0,0))
        padded = cv2.resize(padded, (32,32))
        
        # prepare the padded image for classification via trained model
        padded = padded.astype("float32")/ 255.0
        padded = np.expand_dims(padded,2)
        
        chars.append((padded,(x,y,w,h)))
    pass

boxes = [b[1] for b in chars]
chars = np.array([c[0] for c in chars],dtype="float32")

preds = model.predict(chars)

# define the list of labels
labelNames = "0123456789"
labelNames += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
labelNames = [l for l in labelNames]

# loop over the predictions and bounding boxes together
for (pred, (x,y,w,h)) in zip(preds, boxes):
    
    i = np.argmax(pred)
    prob = pred[i]
    label = labelNames[i]
    
    print("[INFO] {} - {:.2f}%".format(label, prob*100))
    cv2.rectangle(image, (x,y),(x+w,y+h), (0,255,0),2)
    cv2.putText(image, label, (x-10,y-10),cv2.FONT_HERSHEY_SIMPLEX,1.2,(0,255,0),2)
    
    cv2.imshow("Image",image)
    cv2.waitKey(0)
    pass