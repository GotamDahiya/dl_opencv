# importing the necessary packages
from tensorflow.keras.datasets import mnist
import numpy as np

def load_az_dataset(datasetPath):
    # initialize the list of data and labels
    print('[INFO] loading A-Z dataset')
    data, labels = [], []
    
    # loop over the rows of the A_Z handwritten digit dataset
    for row in open(datasetPath):
        # parse the label and image from the row
        row = row.split(",")
        label= int(row[0])
        image = np.array([int(x) for x in row[1:]], dtype="uint8")
        
        # images are represented as a single channel(gray). Images that are 28x28= 784 pixels -- we
        # need to take this flattened 784-d list of number and reshape them into a 28x28 matrix
        image = image.reshape((28,28))
        
        # update the list of data and labels
        data.append(image)
        labels.append(label)
        pass
    
    # convert the data and labels to numpy arrays
    data = np.array(data, dtype="float32")
    labels = np.array(labels,dtype="int")
    
    # return a 2-tuple of the A-Z data and labels
    return (data, labels)
    pass

def load_mnist_dataset():
    print("[INFO] loading 0-9 MNIST dataset")
    # load the MNIST dataset and stack the training data and testing data together
    ((trainData, trainLabels),(testData,testLabels)) = mnist.load_data()
    data = np.vstack([trainData,testData])
    labels = np.hstack([trainLabels,testLabels])
    
    # return a 2-tuple of the MNIST data
    return (data, labels)
    pass