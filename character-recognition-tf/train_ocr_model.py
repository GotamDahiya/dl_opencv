# importing the required packages
import matplotlib
matplotlib.use("Agg")

from pyimagesearch.models.resnet import ResNet
from pyimagesearch.az_dataset.helpers import load_mnist_dataset
from pyimagesearch.az_dataset.helpers import load_az_dataset
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import SGD

from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

from imutils import build_montages

import matplotlib.pyplot as plt
import numpy as np
import argparse, cv2

ap = argparse.ArgumentParser()
ap.add_argument("-a","--az",required=True,
                help="path to A-Z dataset")
ap.add_argument("-m","--model",type=str,required=True,
                help="path to output trained handwriting recognition model")
ap.add_argument("-p","--plot",type=str,default="plot.png",
                help="path to output training history file")
args = vars(ap.parse_args())

# intiialize the number of epochs to train for, initial learning rate, and batch size
EPOCHS = 50
INIT_LR = 1e-1
BS = 128

# load the A-Z and MNIST datasets
print("[INFO] loading datasets")
(azData, azLabels) = load_az_dataset(args["az"])
(digitData, digitLabels) = load_mnist_dataset()

# the MNIST dataset occupies the labels 0-9, so lets add 10 to every A-Z label to ensure the 
# characters are not incorrectly labeled as digits

azLabels += 10

# stack the A-Z data and labels with the MNIST digits data and labels
data = np.vstack([azData, digitData])
labels = np.hstack([azLabels, digitLabels])

# each image in the A-Z and MNIST digts datasets are 28x28 pixels;
# however, the architecture we're using is designed for 32x32 images,
# so we need to resize them to 32x32
data = [cv2.resize(image, (32, 32)) for image in data]
data = np.array(data, dtype="float32")

# add a channel dimension to every image in the dataset and scale the pixel intensities of the image
# from [0,255] to [0,1]
data = np.expand_dims(data, axis=-1)
data /= 255.0

# convert the labels from integers to vectors
le = LabelBinarizer()
labels = le.fit_transform(labels)
counts = labels.sum(axis=0)

# account for skew in the labeled data
classTotals = labels.sum(axis=0)
classWeight = {}

# loop over all classes and calculate the class weight
for i in range(0, len(classTotals)):
    classWeight[i] = classTotals.max() / classTotals[i]
    pass

# partition the data into training and testing splits using 80% of data for training and 20% of 
# data for testing
(trainX, testX, trainY,testY) = train_test_split(data, labels, test_size=0.20,stratify=labels,
                                                 random_state=42)

aug = ImageDataGenerator(
    rotation_range=10,
    zoom_range=0.05,
    width_shift_range=0.1,
    height_shift_range=0.1,
    shear_range=0.15,
    horizontal_flip=False,
    fill_mode="nearest"
    )

# initialize and compile our deep neural network
print("[INFO] compiling model")
opt = SGD(lr=INIT_LR,decay=INIT_LR/EPOCHS)
model = ResNet.build(32, 32, 1, len(le.classes_), (3,3,3),(64,64,128,128),reg=0.0005)
model.compile(loss="categorical_crossentropy",optimizer=opt,metrics=['accuracy'])

# train the network
print("[INFO] training the network")
history = model.fit(
    aug.flow(trainX, trainY, batch_size=BS),
    validation_data=(testX, testY),
    steps_per_epoch=len(trainX)//BS,
    epochs=EPOCHS,
    class_weight=classWeight,
    verbose=1)

# define the list of label names
labelNames ="0123456789"
labelNames = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
labelNames = [l for l in labelNames]

# evaluate the network
print("[INFO] evaluating network")
predictions = model.predict(testX,batch_size=BS)
print(classification_report(testY.argmax(axis=1),
                            predictions.argmax(axis=1),
                            target_names=labelNames))

# save the model to disk
print('[INFO] serialising network')
model.save(args["model"], save_format="h5")

# construct a plot that plots and saves the training history
N = np.arange(0, EPOCHS)
plt.style.use("ggplot")
plt.figure()
plt.plot(N, history.history["loss"], label="training loss")
plt.plot(N, history.history["val_loss"], label="testing loss")
plt.plot(N, history.history["accuracy"], label="training accuracy")
plt.plot(N, history.history["val_accuracy"], label="testing accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(args["plot"])

# initialize the list of output test images
images = []

# randomly select a few testing characters
for i in np.random.choice(np.arange(0,len(testY)), size=(49,)):
    # classify the character
    probs = model.predict(testX[np.newaxis, i])
    prediction = probs.argmax(axis=1)
    label = labelNames[prediction][0]
    
    # extract the image from the test dataset and initialize the text label colour as green(correct)
    # or red(incorrect)
    image = (testX[i]*255).astype("uint8")
    colour = (0,255,0)
    
    if prediction[0] != np.argmax(testY[i]):
        colour = (0,0,255)
        
    # merge the channels into one image, resize the image from 32x32 to 96x96 so we can better see 
    # it and then draw the predicted label on the image
    image = cv2.merge([image] * 3)
    image = cv2.resize(image, (96,96), interpolation=cv2.INTER_LINEAR)
    cv2.putText(image, label, (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, colour, 2)
    
    # add the image to the list of output images
    images.append(image)
    pass

montage = build_montages(images, (96,96), (7,7))[0]

cv2.imshow("OCR Results", montage)
cv2.waitKey(0)