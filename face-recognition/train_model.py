#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 12:46:27 2020

@author: gotam_dahiya
"""


from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
import argparse, pickle

# constructing the argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-e","--embeddings",required=True,
                help="path to embedding file")
ap.add_argument("-r","--recognizer",required=True,
                help="path to output model trained to recognize faces")
ap.add_argument("-l","--le",required=True,
                help="path to label encoder")
args = vars(ap.parse_args())

print("[INFO] loading face embeddings")
data = pickle.loads(open(args["embeddings"],"rb").read())

# encode the labels
print("[INFO] encoding labels")
le = LabelEncoder()
labels = le.fit_transform(data["names"])

# Training the model to accept the 128-d embeddings of the face
print("[INFO] training model")
recognizer = SVC(C=1.0, kernel="linear",probability=True)
recognizer.fit(data["embeddings"], labels)

# Writing the actual face recognition model to disk
f = open(args["recognizer"],"wb")
f.write(pickle.dumps(recognizer))
f.close()

# write the label encoder to disk
f = open(args["le"],"wb")
f.write(pickle.dumps(le))
f.close()

print("[END] finished writing the pickle files to disk")