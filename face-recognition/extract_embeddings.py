from imutils import paths
import numpy as np
import argparse, imutils, pickle, cv2, os

ap = argparse.ArgumentParser()
ap.add_argument("-i","--dataset",required=True,
                help="path to input directory of faces + images")
ap.add_argument("-e","--embeddings",required=True,
                help="path to output serialized db of facial images")
ap.add_argument("-d","--detector",required=True,
                help="path to OpenCV's deep learning face detector")
ap.add_argument("-m","--embedding-model",required=True,
                help="path to OpenCV's deep learning face embedding model")
ap.add_argument("-c","--confidence",type=float,default=0.5,
                help="minimum probability to filter weak detections")

args = vars(ap.parse_args())

# Load serialized face detector from disk
print("[INFO] loading face detector")
protoPath = os.path.sep.join([args["detector"],"deploy.prototxt"])
modelPath = os.path.sep.join([args["detector"],
                          "res10_300x300_ssd_iter_140000.caffemodel"])
detector = cv2.dnn.readNetFromCaffe(protoPath,modelPath)

# Load the serialized face embedding model from disk
print("[INFO] loading face recognizer")
embedder = cv2.dnn.readNetFromTorch(args["embedding_model"])

print("[INFO] quantifying faces")
imagePaths = list(paths.list_images(args["dataset"]))

knownEmbeddings = []
knownNames = []

total = 0

# Loop over image paths
for (i,image_path) in enumerate(imagePaths):
    
    print("[INFO] processing image {}/{}".format(i+1,len(imagePaths)))
    name = image_path.split(os.path.sep)[-2]
    
    # Load the image, resize it to have a width of 600 pixels while maintaing 
    # aspect ratio
    image = cv2.imread(image_path)
    image = imutils.resize(image,width=600)
    (h,w) = image.shape[:2]
    
    imageBlob = cv2.dnn.blobFromImage(
        cv2.resize(image,(300,300)),1.0,(300,300),
        (104.0,177.0,123.0),swapRB=False,crop=False)
    
    detector.setInput(imageBlob)
    detections = detector.forward()
    
    # Ensuring at least one face was found
    if len(detections) > 0:
        # Assumption that each image has only one face
        # Finding bounding box with the largest probability
        
        i = np.argmax(detections[0,0,:,2])
        confidence = detections[0,0,i,2]
        
        # ensuring that the confidence is greater than least confidence value
        if confidence > args["confidence"]:
            # Compute the (x,y) coordinates of the bounding box for the face
            box = detections[0,0,i,3:7]*np.array([w,h,w,h])
            (startX,startY,endX,endY) = box.astype("int")
            
            # Extract the face ROI and grab the ROI dimensions
            face = image[startY:endY,startX:endX]
            (fH, fW) = face.shape[:2]
            
            # ensure the face width and height are sufficiently large
            if fW < 20 or fH < 20:
                continue
            
            # Construct a blob for the face ROI, then pass the blob through
            # our face embedding model to obtain the 128-d quantification of the face
            faceBlob = cv2.dnn.blobFromImage(face,1.0/255,(96,96),(0,0,0),
                                             swapRB=True,crop=False)
            embedder.setInput(faceBlob)
            vec = embedder.forward()
            
            # add the image of the person + face embeddings to the respective lists
            knownNames.append(name)
            knownEmbeddings.append(vec.flatten())
            total += 1
    pass

print("[INFO] starting serializing {} encodings".format(total))
data = {"embeddings":knownEmbeddings,"names":knownNames}
f = open(args["embeddings"],"wb")
f.write(pickle.dumps(data))
f.close()
print("[END] finished creating the 128-d serialized embeddings")