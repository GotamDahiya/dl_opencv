from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse, imutils, pickle, time, cv2, os

# Getting arguments from the command line
ap = argparse.ArgumentParser()
ap.add_argument("-d","--detector",required=True,
                help="path to OpenCv's deep learning face detector")
ap.add_argument("-m","--embedding-model",required=True,
                help="Path to OpenCV's deep learning face embedding model")
ap.add_argument("-r","--recogniser",required=True,
                help="path to model trained to recognise faces")
ap.add_argument("-l","--le",required=True,
                help="path to label encoder")
ap.add_argument("-c","--confidence",type=float,default=0.5,
                help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# Loading serialised face detector from disk
print("[INFO] loading face detector")
protoPath = os.path.sep.join([args["detector"],"deploy.prototxt"])
modelPath = os.path.sep.join([args["detector"],"res10_300x300_ssd_iter_140000.caffemodel"])
detector = cv2.dnn.readNetFromCaffe(protoPath,modelPath)

# Loading serialised face embedding model from disk
print("[INFO] loading face recogniser")
embedder = cv2.dnn.readNetFromTorch(args["embedding_model"])

# load the actual face recognition model with label encoder
recogniser = pickle.loads(open(args["recogniser"],"rb").read())
le = pickle.loads(open(args["le"],"rb").read())

# starting the video stream
print("[INFO] starting video stream")
vs = VideoStream(src=0).start()
time.sleep(2.0)

fps = FPS().start()

while True:
    
    frame = vs.read()
    frame = imutils.resize(frame,width=600)
    (h,w) = frame.shape[:2]
    
    imageBlob = cv2.dnn.blobFromImage(
        cv2.resize(frame,(300,300)),1.0,(300,300),
        (104.0,177.0,123.0),swapRB=False,crop=False)
    
    detector.setInput(imageBlob)
    detections = detector.forward()
    
    for i in range(0,detections.shape[2]):
        
        confidence = detections[0,0,i,2]
        
        if confidence > args["confidence"]:
            box = detections[0,0,i,3:7]*np.array([w,h,w,h])
            (startX,startY,endX,endY) = box.astype("int")
            
            face = frame[startY:endY,startX:endX]
            (fH,fW) = face.shape[:2]
            
            if fW<20 or fH<20:
                continue
            
            faceBlob = cv2.dnn.blobFromImage(face,1.0/255,(96,96),(0,0,0),
                                             swapRB=True,crop=False)
            embedder.setInput(faceBlob)
            vec = embedder.forward()
            
            preds = recogniser.predict_proba(vec)[0]
            j = np.argmax(preds)
            proba = preds[j]
            name = le.classes_[j]
            
            text = "{}: {:.2f}%".format(name,proba*100)
            y = startY-10 if startY-10>10 else startY+10
            cv2.rectangle(frame,(startX,startY),(endX,endY),(0,255,0),2)
            cv2.putText(frame,text,(startX,y),cv2.FONT_HERSHEY_SIMPLEX,0.45,
                        (255,0,0),2)
        pass
    fps.update()
    
    cv2.imshow("Frame",frame)
    key = cv2.waitKey(1) & 0xFF
    
    if key == ord('q'):
        break
    pass

fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

cv2.destroyAllWindows()
vs.stop()